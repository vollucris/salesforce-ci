### Salesforce DX Docker image ###

* This is a new Docker image including the Salesforce DX CLI tool for further CI automation
* Current Version: 1.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

#### Revision History ####
Version: 1.1
- added the sfdx update into the image so that it isn't running when the authenticatication is triggered in the first CI Build.